// swift-tools-version:5.8
import PackageDescription

let package = Package(
    name: "Debug",
    platforms: [
        .iOS(.v16),
        .macOS(.v13)
    ],
    products: [
        .library(
            name: "Debug",
            targets: ["Debug"]
        ),
    ],
    dependencies: [
        .package(
            url: "https://github.com/Liftric/DIKit.git",
            exact: "1.6.1"
        ),
        .package(url: "https://github.com/kean/Pulse.git", .upToNextMajor(from: "4.0.0")),
        .package(
            url: "https://gitlab.com/Blockdaemon/wallet-sdk/ios/NetworkKit",
            exact: "1.0.0"
        ),
    ],
    targets: [
        .target(
            name: "Debug",
            dependencies: [
                "NetworkKit",
                .product(name: "Pulse", package: "Pulse"),
                .product(name: "PulseUI", package: "Pulse"),
                .product(name: "DIKit", package: "DIKit")
            ],
            path: "Sources/Debug"
        )
    ]
)
