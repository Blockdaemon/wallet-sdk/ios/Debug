// Copyright © Blockdaemon All rights reserved.

import DIKit
import Foundation
import NetworkKit

extension DependencyContainer {
    public static var debug = module {
        
        single {
            PulseNetworkDebugScreenProvider() as NetworkDebugScreenProvider
        }

        single {
            PulseNetworkDebugLogger() as NetworkDebugLogger
        }
    }
}
