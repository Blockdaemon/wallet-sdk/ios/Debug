// Copyright © Blockdaemon All rights reserved.

import Foundation
import NetworkKit
import Pulse

final class PulseNetworkDebugLogger: NetworkDebugLogger {

    func storeRequest(
        _ request: URLRequest,
        response: URLResponse?,
        error: Error?,
        data: Data?,
        metrics: URLSessionTaskMetrics?,
        session: URLSession?
    ) {
        LoggerStore.shared.storeRequest(
            request,
            response: response,
            error: error,
            data: data,
            metrics: metrics
        )
    }

    func storeRequest(
        _ request: URLRequest,
        result: Result<URLSessionWebSocketTask.Message, Error>,
        session: URLSession?
    ) {
        switch result {
        case .success(let message):
            switch message {
            case .data(let data):
                storeRequest(
                    request,
                    response: nil,
                    error: nil,
                    data: data,
                    metrics: nil,
                    session: session
                )
            case .string(let string):
                storeRequest(
                    request,
                    response: nil,
                    error: nil,
                    data: string.data(using: .utf8),
                    metrics: nil,
                    session: session
                )
            @unknown default:
                // No action
                break
            }
        case .failure(let failure):
            storeRequest(
                request,
                response: nil,
                error: failure,
                data: nil,
                metrics: nil,
                session: session
            )
        }
    }
}
