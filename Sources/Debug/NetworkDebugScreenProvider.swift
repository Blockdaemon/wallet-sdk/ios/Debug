// Copyright © Blockdaemon All rights reserved.

import DIKit
import Foundation
import SwiftUI
import PulseUI

public protocol NetworkDebugScreenProvider {
    @ViewBuilder func buildDebugView() -> AnyView
}

public struct Pulse: View {
    @Inject var networkDebugScreenProvider: NetworkDebugScreenProvider
    
    public init() { }

    public var body: some View {
        networkDebugScreenProvider.buildDebugView()
    }
}
 
final class PulseNetworkDebugScreenProvider: NetworkDebugScreenProvider {
    @ViewBuilder func buildDebugView() -> AnyView {
        #if os(iOS)
        AnyView(
            ConsoleView()
                .closeButtonHidden()
        )
        #else
        AnyView(
            ConsoleView()
        )
        #endif
    }
}


