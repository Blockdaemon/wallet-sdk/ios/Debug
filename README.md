# Debug

This package includes `Pulse` as well as the `NetworkDebugScreenProvider`. It's used for launching the `Debug` view and displays all networking events. Given the SDK doesn't provide UI to clients, this can be handy to "show" that features are implemented without having to build much UI. 
